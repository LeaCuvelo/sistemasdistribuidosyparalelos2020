#include<stdio.h>
#include<stdlib.h>

/* Time in seconds from some point in the past */
double dwalltime();

int main(int argc,char*argv[]){
 double *A,*B,*C,*D,*Temp;
 int i,j,k,N;
 int check=1;
 double timetick;
 register int reg = 0;

 //Controla los argumentos al programa
  if (argc < 2){
   printf("\n Falta un argumento:: N dimension de la matriz \n");
   return 0;
  }
 
   N=atoi(argv[1]);

 //Aloca memoria para las matrices
  A=(double*)malloc(sizeof(double)*N*N);
  B=(double*)malloc(sizeof(double)*N*N);
  C=(double*)malloc(sizeof(double)*N*N);
  D=(double*)malloc(sizeof(double)*N*N);  
  Temp=(double*)malloc(sizeof(double)*N*N);  

 
  for(i=0;i<N;i++){
   for(j=0;j<N;j++){
	A[i*N+j]=1;
	B[i+j*N]=1;
	C[i*N+j]=1;
	D[i*N+j]=0;    	
    Temp[i*N+j]=0; 
   }
  }   

  timetick = dwalltime();
 //Realiza la multiplicación
  for(i=0;i<N;i++){
   for(j=0;j<N;j++){
    for(k=0;k<N;k++){
        reg += A[i*N+k]*B[k+j*N];
	}
    Temp[i*N+j] = reg;
    reg = 0;
   }
  }  

   for(i=0;i<N;i++){
   for(j=0;j<N;j++){
    for(k=0;k<N;k++){
        reg += Temp[i*N+k]*C[k+j*N];
	}
    D[i*N+j] = reg;
    reg = 0;
   }
  }  
   
  printf("Tiempo en segundos %f \n", dwalltime() - timetick);

 //Verifica el resultado
  for(i=0;i<N;i++){
   for(j=0;j<N;j++){
	check=check&&(D[i*N+j]==N*N);
   }
  }   

  if(check){
   printf("Multiplicacion de matrices resultado correcto\n");
  }else{
   printf("Multiplicacion de matrices resultado erroneo\n");
  }

 free(A);
 free(B);
 free(C);
 free(D); 
 free(Temp);
 return(0);
}



/*****************************************************************/

#include <sys/time.h>

double dwalltime()
{
	double sec;
	struct timeval tv;

	gettimeofday(&tv,NULL);
	sec = tv.tv_sec + tv.tv_usec/1000000.0;
	return sec;
}

