#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include<omp.h>

//Variables globales
int N = 4;
double* A;
double* B;
double* B_Parcial;
double* C;
double* D;
double timetick;
int i, j, k;      //Variables indice para el manejo de la matriz.
int check = 1;


//Funcion para calcular el tiempo.
double dwalltime(){
    double sec;
    struct timeval tv;
    gettimeofday(&tv,NULL);
    sec = tv.tv_sec + tv.tv_usec/1000000.0;
    return sec;
}

//Funciones seriales
void print_result();
void print_A_resultPorFila();
void print_B_resultPorCol();
void print_C_resultPorCol();


int main(int argc, char* argv[]){

  //controlamos los parametros recibidos
  if (argc < 2){
		printf("\n Falta un parametro ");
		printf("\n 1. Dimension de la matriz ");
		return 0;
	}
	N = atoi(argv[1]);			//Primer parametro, tamano de las Matrices.


  
  A=(double*)malloc(sizeof(double)*N*N);
  B=(double*)malloc(sizeof(double)*N*N);
  B_Parcial=(double*)malloc(sizeof(double)*N*N);
  C=(double*)malloc(sizeof(double)*N*N);
  D=(double*)malloc(sizeof(double)*N*N);



  //Init de matrices
  for(int i=0;i<N;i++){
   for(int j=0;j<N;j++){
     A[i*N+j]= 1;
     B[i+j*N]= 1;
     C[i+j*N]= 1;
     D[i*N+j]= 0;
     B_Parcial[i*N+j]=0;
    }
  }

//Se podría implementar una mejora realizando un collapse(3), pero operando de manera correcta con
// las lineas de asignación de suma como es B_Parcial[i*N+j] = reg;
  timetick = dwalltime();
  #pragma omp parallel private(i,j,k)
  {    
    register int reg = 0;
    #pragma omp for collapse(2)
      for(i=0;i<N;i++){
        for(j=0;j<N;j++){
          for(k=0;k<N;k++){
            reg += A[i*N+k] * B[k+j*N];
          }
          B_Parcial[i*N+j] = reg;
          reg = 0; 
        }
      } 
  }
 
  #pragma omp parallel private(i,j,k)
  {    
    register int reg = 0;
    #pragma omp for collapse(2)
      for(i=0;i<N;i++){
        for(j=0;j<N;j++){
          for(k=0;k<N;k++){
            reg += B_Parcial[i*N+k] * C[k+j*N];
          }
          D[i*N+j] = reg;
          reg = 0; 
        }
      } 
  }  
 
 printf("Tiempo en segundos para el thread  %f \n", dwalltime() - timetick);

//Verifica el resultado checkeado
  for(i=0;i<N;i++){    
    for(j=0;j<N;j++){    
      check=check&&(D[i*N+j]==N*N);       
    }
  }  
  
  if(check){   
    printf("Resultado de Multiplicación CORRECTO \n");
  }
  else{
    printf("Resultado de Multiplicación INCORRECTO \n");
  }
  //Liberación de memoria
  free(A);
  free(B);
  free(C);
  free(B_Parcial);
  free(D);

  return 0;
}   
