#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>

//Variables globales
int num_of_threads;
int *Arreglo;
int valor, N;
double divisor = 1;
int cantidad = 0;
double timetick;
pthread_mutex_t miMutex = PTHREAD_MUTEX_INITIALIZER;

//Funcion paralela
void* buscar(void *arg);

//Funcion para calcular el tiempo.
double dwalltime(){
    double sec;
    struct timeval tv;
    gettimeofday(&tv,NULL);
    sec = tv.tv_sec + tv.tv_usec/1000000.0;
    return sec;
}

int main(int argc, char* argv[]){

    //controlamos los parametros recibidos
    if (argc < 3){
        printf("\n Falta un parametro ");
        printf("\n 1. Valor a encontrar");
        printf("\n 2. N numeros");
        printf("\n 3. Numero de hilos ");
        return 0;
    }

    valor = atoi(argv[1]);			//Primer parametro, Valor  a encontrar
    N = atoi(argv[2]);		//Segundo parametro, cantidad de elementos
    num_of_threads = atoi(argv[3]);	//Tercer parametro, numero de Threads.

    pthread_t hilos[num_of_threads];
    int id_hilos[num_of_threads];

    switch(num_of_threads)
    {
        case 2:
        divisor = 0.5;
        break;
        case 4:
        divisor = 0.25;
        break;
        case 8:
        divisor = 0.125;
        break;
    }

    Arreglo=(int*)malloc(sizeof(int)*N);

    //Cargar el vector N elementos
    for(int i = 0; i< N; i++){
        Arreglo[i] = rand()%10;
    }
    //Comienza a contar el tiempo
    timetick = dwalltime();
    //Creacion de threads.
    for(int i=1;i<num_of_threads;i++){
        id_hilos[i]=i;
        pthread_create(&hilos[i], NULL, &buscar, (void *) &id_hilos[i]);
    }
    buscar(&id_hilos[0]);

    //Esperar que termines los hilos.
    for(int i=1;i<num_of_threads;i++){
        pthread_join(hilos[i], NULL);
    }
    printf("Tiempo en segundos %f\n", dwalltime() - timetick);
    printf("Cantidad %d\n", cantidad);

    


    pthread_mutex_destroy(&miMutex);


return 0;
}

    void* buscar(void *arg  ){

        int i;
        int id = *(int*)arg;
        int inicio = (id * N)*divisor;
        int fin = ((id+1)*N)*divisor  - 1;
        register int cantLocal=0;

        for(i = inicio; i<= fin; i++){
            if(Arreglo[i] == valor){ 
                cantLocal++;
            }
        }

        //Exclusion mutua
        if (pthread_mutex_trylock(&miMutex) == 0){ 
            cantidad = cantidad + cantLocal;
            pthread_mutex_unlock(&miMutex);
        }    

    }
