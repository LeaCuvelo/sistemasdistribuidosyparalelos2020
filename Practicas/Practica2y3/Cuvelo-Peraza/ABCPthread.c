#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>

//Variables globales
int num_of_threads;
int N = 4;
double* A;
double* B;
double* B_Parcial;
double* C;
double* D;
double timetick;
double divisor = 1;
int check = 1;



//Funcion paralela
void* matMul(void *arg);
//Función para calcular el tiempo.
double dwalltime(){
    double sec;
    struct timeval tv;
    gettimeofday(&tv,NULL);
    sec = tv.tv_sec + tv.tv_usec/1000000.0;
    return sec;
}

int main(int argc, char* argv[]){

  //controlamos los parametros recibidos
  if (argc < 3){
		printf("\n Falta un parametro ");
		printf("\n 1. Dimension de la matriz ");
		printf("\n 2. Numero de hilos ");
	  	return 0;
	}
	N = atoi(argv[1]);			//Primer parametro, tamano de las Matrices.
	num_of_threads = atoi(argv[2]);	//Segundo parametro, numero de Threads.

// EL divisor quedaría acotado a 2, 4 y 8 threads
  switch(num_of_threads)
  {
    case 2:
      divisor = 0.5;
      break;

    case 4:
      divisor = 0.25;
      break;
    case 8:
      divisor = 0.125;
      break;
  }

  pthread_t hilos[num_of_threads];
  int i, j, id_hilos[num_of_threads];

  A=(double*)malloc(sizeof(double)*N*N);
  B=(double*)malloc(sizeof(double)*N*N);
  B_Parcial=(double*)malloc(sizeof(double)*N*N);
  C=(double*)malloc(sizeof(double)*N*N);
  D=(double*)malloc(sizeof(double)*N*N);


	
  //Init de matrices
  for(i=0;i<N;i++){
   for(j=0;j<N;j++){
     A[i*N+j]= 1;
     B[i+j*N]= 1;
     C[i+j*N]= 1;
     D[i*N+j]= 0;
     B_Parcial[i*N+j]=0;
    }
  }
    timetick = dwalltime();
	//Creacion de threads.
	for(i=1;i< num_of_threads;i++){
		id_hilos[i]=i;
		pthread_create(&hilos[i], NULL, &matMul, (void *) &id_hilos[i]);
	}
    id_hilos[0] = 0;    
    matMul(&id_hilos[0]);

	//Esperar que terminen los hilos.
    for(i=1;i< num_of_threads;i++){
		pthread_join(hilos[i], NULL);
	}
  
	printf("Tiempo en segundos %f\n", dwalltime() - timetick);
  //Verifica el resulcheckado   
  for(i=0;i<N;i++){    
    for(j=0;j<N;j++){    
      check=check&&(D[i*N+j]==N*N);       
    }
  }  
  
  if(check){   
    printf("Resultado de Multiplicación CORRECTO \n");
  }
  else{
    printf("Resultado de Multiplicación INCORRECTO \n");
  }
      

  //Liberación de memoria
  
  free(A);
  free(B);
  free(C);
  free(B_Parcial);
  free(D);  

  return 0;
}

//-------------------------------------------
//Realiza la multiplicacion parcial
void* matMul(void *arg){
	int id = *(int*)arg, i, j, k; 
	int inicio=(id*N)*divisor;
	int fin=((id+1)*N)*divisor;
  register int reg = 0;

  //A*B
	for (i=inicio;i<fin;i++){
		for (j=0;j<N;j++){
			for (k=0;k<N;k++){
        reg += A[i*N+k]*B[k+j*N];
			}
      B_Parcial[i*N+j] = reg;
      reg = 0;
		}	
	}

  //B_Parcial*C
	for (i=inicio;i<fin;i++){
		for (j=0;j<N;j++){
			for (k=0;k<N;k++){
        reg += B_Parcial[i*N+k]*C[k+j*N];
			}
      D[i*N+j] = reg;
      reg = 0;
		}	
	}
}