#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>

//Variables globales
int num_of_threads;
int N = 4;
double* A;
double* B;
double* B_Temporal;
double* C_Temporal;
double* C;
double* D;
double* Result;
double timetick;

//Funciones seriales
void print_result();
void print_A_resultPorFila();
void print_B_resultPorCol();
void print_C_resultPorFila();
void print_D_resultPorCol();
//Funcion paralela
void* matMul(void *arg);

//Funcion para calcular el tiempo.
double dwalltime(){
    double sec;
    struct timeval tv;
    gettimeofday(&tv,NULL);
    sec = tv.tv_sec + tv.tv_usec/1000000.0;
    return sec;
}

int main(int argc, char* argv[]){

  //controlamos los parametros recibidos
  if (argc < 3){
		printf("\n Falta un parametro ");
		printf("\n 1. Dimension de la matriz ");
		printf("\n 2. Numero de hilos ");
	  	return 0;
	}
	N = atoi(argv[1]);			//Primer parametro, tamano de las Matrices.
	num_of_threads = atoi(argv[2]);	//Segundo parametro, numero de Threads.


  pthread_t hilos[num_of_threads];
  int i, j, id_hilos[num_of_threads];

  A=(double*)malloc(sizeof(double)*N*N);
  B=(double*)malloc(sizeof(double)*N*N);
  B_Temporal=(double*)malloc(sizeof(double)*N*N);
  C_Temporal=(double*)malloc(sizeof(double)*N*N);
  C=(double*)malloc(sizeof(double)*N*N);
  D=(double*)malloc(sizeof(double)*N*N);
  Result=(double*)malloc(sizeof(double)*N*N);


	timetick = dwalltime();

  //Init de matrices
  for(i=0;i<N;i++){
   for(j=0;j<N;j++){
     A[i*N+j]= rand()%10;
     B[i+j*N]= rand()%10;
     C[i*N+j]= rand()%10;
     D[i+j*N]= rand()%10;
     B_Temporal[i*N+j]=0;
     C_Temporal[i*N+j]=0;
     Result[i*N+j]=0;
    }
  }

	//Creacion de threads.
	for(i=0;i<num_of_threads;i++){
		id_hilos[i]=i;
		pthread_create(&hilos[i], NULL, &matMul, (void *) &id_hilos[i]);
	}

	//Esperar que termines los hilos.
	for(i=0;i<num_of_threads;i++){
		pthread_join(hilos[i], NULL);
	}

  //Imprimir resultados

  print_A_resultPorFila();
  print_B_resultPorCol();

  print_C_resultPorFila();
  print_D_resultPorCol();

  print_result();


  //Liberación de memoria
  free(A);
  free(B);
  free(C);
  free(D);
  free(B_Temporal);
  free(C_Temporal);
  free(Result);

  printf("Tiempo en segundos %f\n", dwalltime() - timetick);

  return 0;
}

//-------------------------------------------
//Realiza la multiplicación parcial
void* matMul(void *arg){
	int id = *(int*)arg, i, j, k;
	int inicio=(id*N)/num_of_threads;
	int fin=((id+1)*N)/num_of_threads;

  //A*B
	for (i=inicio;i<fin;i++){
		for (j=0;j<N;j++){
			for (k=0;k<N;k++){
				B_Temporal[i*N+j]+=A[i*N+k]*B[k+j*N];
			}
		}	
	}

  //C*D
	for (i=inicio;i<fin;i++){
		for (j=0;j<N;j++){
			for (k=0;k<N;k++){
				C_Temporal[i*N+j]+=C[i*N+k]*D[k+j*N];
			}
		}	
	}
//suma Result
  for (i=inicio;i<fin;i++){
		for (j=0;j<N;j++){
      Result[i*N+j] = B_Temporal[i*N+j] + C_Temporal[i*N+j];
    }
  }

	pthread_exit(NULL);
}



void print_result(){
  int i,j;
  printf("\n Matriz resultado: \n");
  for(i = 0; i < N; i++){
      for(j = 0; j < N; j++){
         printf("%.0f ", Result[i*N+j]);
       }
      printf("\n");
   }
}


void print_A_resultPorFila(){
     int i,j;
     printf("\n Matriz A: \n");
     for(i = 0; i < N; i++){
         for(j = 0; j < N; j++){
            printf("%.0f ",A[i*N+j]);
          }
         printf("\n");
      }
}


void print_B_resultPorCol(){
  int i,j;
  printf("\n Matriz B: \n");
  for(i = 0; i < N; i++){
      for(j = 0; j < N; j++){
         printf("%.0f ",B[i+j*N]);
       }
      printf("\n");
   }

}


void print_C_resultPorFila(){
     int i,j;
     printf("\n Matriz C: \n");
     for(i = 0; i < N; i++){
         for(j = 0; j < N; j++){
            printf("%.0f ",C[i*N+j]);
          }
         printf("\n");
      }
}

void print_D_resultPorCol(){
  int i,j;
  printf("\n Matriz D: \n");
  for(i = 0; i < N; i++){
      for(j = 0; j < N; j++){
         printf("%.0f ",D[i+j*N]);
       }
      printf("\n");
   }

}