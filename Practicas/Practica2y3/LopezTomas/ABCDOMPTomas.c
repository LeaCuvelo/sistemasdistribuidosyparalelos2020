#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include<omp.h>

//Variables globales
int num_of_threads;
int N = 4;
double* A;
double* B;
double* B_Temporal;
double* C_Temporal;
double* C;
double* D; 
double* Result;
double timetick;
int i, j, k;      //Variables indice para el manejo de la matriz.
int check = 1;


//Función para calcular el tiempo.
double dwalltime(){
    double sec;
    struct timeval tv;
    gettimeofday(&tv,NULL);
    sec = tv.tv_sec + tv.tv_usec/1000000.0;
    return sec;
}

int main(int argc, char* argv[]){

  //controlamos los parametros recibidos
  if (argc < 3){
		printf("\n Falta un parametro ");
		printf("\n 1. Dimension de la matriz ");
		printf("\n 2. Numero de hilos ");
	  	return 0;
	}
	N = atoi(argv[1]);			//Primer parametro, tamano de las Matrices.
	num_of_threads = atoi(argv[2]);	//Segundo parametro, numero de Threads.

    omp_set_num_threads(num_of_threads);	//Creacion de los Threads a utilizar.

  
  A=(double*)malloc(sizeof(double)*N*N);
  B=(double*)malloc(sizeof(double)*N*N);
  B_Temporal=(double*)malloc(sizeof(double)*N*N);
  C=(double*)malloc(sizeof(double)*N*N);
  D=(double*)malloc(sizeof(double)*N*N);
  C_Temporal=(double*)malloc(sizeof(double)*N*N);
  Result=(double*)malloc(sizeof(double)*N*N);



  //Init de matrices
  for(int i=0;i<N;i++){
   for(int j=0;j<N;j++){
     A[i*N+j]= 1;
     B[i+j*N]= 1;
     C[i+j*N]= 1;
     D[i*N+j]= 1;
     B_Temporal[i*N+j]=0;
     C_Temporal[i*N+j]=0;
    }
  }

//Se podría implementar una mejora realizando un collapse(3), pero operando de manera correcta con
// las lineas de asignación de suma como es B_Temporal[i*N+j] = reg;
  timetick = dwalltime();
  #pragma omp parallel private(i,j,k) 
  {    
    register int reg = 0;
    #pragma omp for collapse(2) nowait
      for (i=0;i<N;i++){
		for (j=0;j<N;j++){
			for (k=0;k<N;k++){
                reg +=A[i*N+k]*B[k+j*N];
			}
            B_Temporal[i*N+j] = reg;
            reg = 0;
		}	
	}
  }
 
  #pragma omp parallel private(i,j,k) 
  {    
    register int reg = 0;
    #pragma omp for collapse(2)
      for (i=0;i<N;i++){
		for (j=0;j<N;j++){
			for (k=0;k<N;k++){
                reg +=C[i*N+k]*D[k+j*N];
			}
            C_Temporal[i*N+j] = reg;
            reg = 0;
		}	
	}
  }  
  for (i=0;i<N;i++){
		for (j=0;j<N;j++){
            Result[i*N+j] = B_Temporal[i*N+j] + C_Temporal[i*N+j];
        }
    }  	
 
 printf("Tiempo en segundos para el thread  %f \n", dwalltime() - timetick);
 
//Verifica el resultado checkeado
  for(i=0;i<N;i++){    
    for(j=0;j<N;j++){    
      check=check&&(Result[i*N+j]==2*N);       
    }
  }  
  
  if(check){   
    printf("Resultado de Multiplicación CORRECTO \n");
  }
  else{
    printf("Resultado de Multiplicación INCORRECTO \n");
  }
  //Liberación de memoria
  free(A);
  free(B);
  free(C);
  free(B_Temporal);
  free(C_Temporal);
  free(D);
  free(Result);

  return 0;
}   
