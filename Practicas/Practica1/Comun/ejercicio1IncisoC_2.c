#include<stdio.h>
#include<stdlib.h>
#include <sys/time.h>

//Dimension por defecto de las matrices
int N=100;

//Para calcular tiempo
double dwalltime(){
        double sec;
        struct timeval tv;

        gettimeofday(&tv,NULL);
        sec = tv.tv_sec + tv.tv_usec/1000000.0;
        return sec;
}

int main(int argc,char*argv[]){
 double *A,*AT;
 int i,j,k;
 double timetick;

 //Controla los argumentos al programa
 if ((argc != 2) || ((N = atoi(argv[1])) <= 0) )
  {
    printf("\nUsar: %s n\n  n: Dimensión de la matriz (nxn X nxn)\n", argv[0]);
    exit(1);
  }

 //Aloca memoria para las matrices
  A=(double*)malloc(sizeof(double)*N*N);
  AT=(double*)malloc(sizeof(double)*N*N); //AT es la trasposicion de A

 //Inicializa las matrices A y B en i, el resultado sera una matriz con todos sus valores en N
  for(i=0;i<N;i++){
   for(j=0;j<N;j++){
     if(i<=j){
        A[i*N +j]=i+1;
      }else{
        A[i*N+j]=0;	
      }
    }
  }   


  timetick = dwalltime();
//Trasposición 
  for(i=0; i<N; i++){
    for(j=0; j <N ; j++ ){
      AT[i + N*j] = A[i*N +j];
    }
  }

printf("Tiempo en segundos %f\n", dwalltime() - timetick);

//  printf("Valores de la matriz A: \n");
//   for(i=0; i<N; i++){
//     for(j=0; j <N ; j++ ){
//        printf(" %.0f",  A[i*N +j]);
//     }
//     printf("\n");
//   }

//  printf("Valores de la matriz AT \n");
//   for(i=0; i<N; i++){
//     for(j=0; j <N ; j++ ){
//        printf(" %.0f",  AT[i*N +j]);
//     }
//     printf("\n");
//   }

 free(A);
 free(AT);
 return(0);
}
