#include<stdio.h>
#include<stdlib.h>
#include <sys/time.h>
 

int N;

//Para calcular tiempo
double dwalltime(){
        double sec;
        struct timeval tv;

        gettimeofday(&tv,NULL);
        sec = tv.tv_sec + tv.tv_usec/1000000.0;
        return sec;
}

int main(int argc,char*argv[]){
 double *A,*B,*C;
 int i,j,k;
 double timetick;

 //Controla los argumentos al programa
 if ((argc != 2) || ((N = atoi(argv[1])) <= 0) )
  {
    printf("\nUsar: %s n\n  n: Dimension de la matriz (nxn X nxn)\n", argv[0]);
    exit(1);
  }

 //Aloca memoria para las matrices
  A=(double*)malloc(sizeof(double)*N*N);
  B=(double*)malloc(sizeof(double)*N*N);
  C=(double*)malloc(sizeof(double)*N*N);

 //Inicializa las matrices A y B
  for(i=0;i<N;i++){
   for(j=0;j<N;j++){
      A[i*N+j] = rand()%10;
      B[i + N*j] = rand()%10;
	}
  }   


 //Realiza la multiplicación

  timetick = dwalltime();

  for(i=0;i<N;i++){
   for(j=0;j<N;j++){
    C[i*N+j] = 0;
    for(k=0;k<N;k++){
       C[i*N+j] = C[i*N+j] + (A[i*N+k] * B[k+N*j]);
    }
   }
  }   

 printf("Tiempo en segundos %f\n", dwalltime() - timetick);

//Descomentar si se quiere imprimir los valores de las matrices
/*
printf("Imprimo C \n");
for(i=0;i<N;i++){
   for(j=0;j<N;j++){
      printf("%.0f ", C[i*N+j]);  
	}
   printf("\n");
  }   
  printf("\n");
printf("Imprimo A \n");
  for(i=0;i<N;i++){
   for(j=0;j<N;j++){
      printf("%.0f ", A[i*N+j]);  
	}
   printf("\n");
  }   
  printf("\n");

printf("Imprimo B \n");
  for(i=0;i<N;i++){
   for(j=0;j<N;j++){
      printf("%.0f ", B[i + N*j]);
	}
   printf("\n");
  }   
 */
 free(A);
 free(B);
 free(C);
 return(0);
}
