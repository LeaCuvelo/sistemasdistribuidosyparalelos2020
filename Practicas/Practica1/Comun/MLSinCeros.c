#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

int N;

//Para calcular tiempo
double dwalltime(){
        double sec;
        struct timeval tv;

        gettimeofday(&tv,NULL);
        sec = tv.tv_sec + tv.tv_usec/1000000.0;
        return sec;
}

int main(int argc,char* argv[]){

 double *M,*L,*C;
 int i,j,k;
 int check = 1;
 double timetick;
 double H;

 //Controla los argumentos al programa
 if ((argc != 2) || ((N = atoi(argv[1])) <= 0) ) {
    printf("\nUsar: %s n\n  n: Dimension de la matriz (nxn X nxn)\n", argv[0]);
    exit(1);
  }
 
 //Aloca memoria para las matrices
 H = (N*N-(N*(N-1))/2);
 M=(double*)malloc(sizeof(double)*N*N);
 L=(double*)malloc(sizeof(double)*H);
 C=(double*)malloc(sizeof(double)*N*N);

 //Inicializa las matrices
 //La matriz M se inicializa toda con rand()%10
 //La matriz L se inicializa triangular inferior con N
 //La matriz C se inicializa en 0
 for(i=0;i<N;i++){
  for(j=0;j<N;j++){
	M[i*N+j]=rand()%10;
	C[i*N+j]=0;	
  }
 }

//Icializamos L
for(i=0;i<N;i++){
  for(j=0;j<N;j++){

	L[]= M[k+j*N-(k*(k+1)/2)];	// Aca no pudimos encontrar un algoritmo que nos inicialice la matriz L
  }
 }

/*
	1 0 0
	4 4 0
	7 6 9
*/

 //Realiza la multiplicación 
 timetick = dwalltime();

 for(i=0;i<N;i++){
  for(j=0;j<N;j++){
   for(k=0;k<N;k++){
    C[i*N+j]= C[i*N+j] + M[i*N+k]*L[ ]; 	// y aca tampoco encontramos la manera de manipular L
   }
  }
 }

 printf("Tiempo en segundos %f\n", dwalltime() - timetick);

 
 
 
 free(M);
 free(L);
 free(C);

 return(0);
}
