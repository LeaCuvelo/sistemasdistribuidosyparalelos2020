#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

int N;

//Para calcular tiempo
double dwalltime(){
        double sec;
        struct timeval tv;

        gettimeofday(&tv,NULL);
        sec = tv.tv_sec + tv.tv_usec/1000000.0;
        return sec;
}

int main(int argc,char* argv[]){

 double *M,*L,*C;
 int i,j,k;
 int check = 1;
 double timetick;

 //Controla los argumentos al programa
 if ((argc != 2) || ((N = atoi(argv[1])) <= 0) ) {
    printf("\nUsar: %s n\n  n: Dimension de la matriz (nxn X nxn)\n", argv[0]);
    exit(1);
  }
 
 //Aloca memoria para las matrices
 M=(double*)malloc(sizeof(double)*N*N);
 L=(double*)malloc(sizeof(double)*N*N);
 C=(double*)malloc(sizeof(double)*N*N);

 //Inicializa las matrices
 //La matriz M se inicializa en valores random
 //La matriz L se inicializa triangular inferior con N
 //La matriz C se inicializa en 0

 //Ejemplo

 /*
 
 M  = ordenado x filas
 4 2 3
 5 1 1
 6 8 9

En memoria. 4 2 3 5 1 1 6 8 9


L = ordenado por columnas

N 0 0
N N 0
N N N

En memoria: N N N 0 N N 0 0 N

 */

 for(i=0;i<N;i++){
  for(j=0;j<N;j++){

    M[i*N+j]= rand() % 10;
   
   //Almacena los elementos inferiores  a la diagonal pcpal.
   if(i>=j){
    L[i+N*j]=N;
   }else{
    L[i+N*j]=0;	//Esta almacenando los ceros
   }

   C[i*N+j]=0;	
  }
 }


 //Realiza la multiplicacion 
 timetick = dwalltime();

 for(i=0;i<N;i++){
  for(j=0;j<N;j++){
   for(k=0;k<N;k++){
    C[i*N+j]=C[i*N+j] + M[i*N+k] * L[k+j*N];
   }
  }
 }

//Descomentar las lineas de abajo para ver los valores de las matrices M, L y C

//  printf("Matriz M: \n");

//  for(i=0;i<N;i++){
//   for(j=0;j<N;j++){
//         printf("%f ", M[i*N +j]);
//   }
//   printf("\n");
// }    

//  printf("Matriz L: \n");

//  for(i=0;i<N;i++){
//   for(j=0;j<N;j++){
//         printf("%f ", L[i + j*N]);
//   }
//   printf("\n");
// }    

//  printf("Matriz C: \n");

//  for(i=0;i<N;i++){
//   for(j=0;j<N;j++){
//         printf("%f ", C[i*N +j]);
//   }
//   printf("\n");
// }   


 printf("Tiempo en segundos %f\n", dwalltime() - timetick);


 free(M);
 free(L);
 free(C);

 return(0);
}
