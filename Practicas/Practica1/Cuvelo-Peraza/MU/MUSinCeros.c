#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

int N;

//Para calcular tiempo
double dwalltime(){
        double sec;
        struct timeval tv;

        gettimeofday(&tv,NULL);
        sec = tv.tv_sec + tv.tv_usec/1000000.0;
        return sec;
}

void imprimirMatriz(double *aux, int lado){
  int i,j;
  for (i = 0; i < lado; i++){
    for (j = 0; j < lado; j++){
      printf("%.0f\t", aux[i*N+j] );
    }
    printf("\n");
  }
}

void imprimirMatrizTriangular(double *aux, int lado){
  int i,j;
  for (i = 0; i < lado; i++){
    for (j = 0; j < lado; j++){
      if(i<=j){
        printf("%.0f\t", aux[i+j*((j+1)/2)] );
      }
      else
        printf("0\t");
    }
    printf("\n");
  }
}
int main(int argc,char* argv[]){
  double *M,*U,*C;
  int i,j,k;
  double timetick;
  int H;

  //Controla los argumentos al programa
  if ((argc != 2) || ((N = atoi(argv[1])) <= 0) ) {
      printf("\nUsar: %s n\n  n: Dimension de la matriz (nxn X nxn)\n", argv[0]);
      exit(1);
    }

  H = (N*N-(N*(N-1))/2);
  M=(double*)malloc(sizeof(double)*N*N);
  U=(double*)malloc(sizeof(double)*H);
  C=(double*)malloc(sizeof(double)*N*N);

  for(i=0;i<N;i++){
      for(j=0;j<N;j++){
        M[i*N+j]=1;
        C[i*N+j]=0;
        U[i+j*((j+1)/2)]= 1; 
    }
  }

  /* Realiza la multiplicación */
  timetick = dwalltime();
  for(i=0;i<= N -1;i++){
    for(j=0;j<= N-1;j++){
    for(k=0;k<=j;k++){
      C[i*N+j] += M[i*N+k]*U[i+(k*((k+1)/2))];    
    }
    }
  }
  //Descomentar si se quiere imprimir el valor de las matrices
  // imprimirMatriz(M,N);
  // printf("\n");
  // printf("\n");
  // imprimirMatrizTriangular(U,N);
  // printf("\n");
  // printf("\n");
  // imprimirMatriz(C,N);

  printf("Tiempo en segundos %f\n", dwalltime() - timetick);
  free(M);
  free(U);
  free(C);

  return(0);
}
