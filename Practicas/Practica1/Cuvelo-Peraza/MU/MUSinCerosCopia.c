#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

int N;

//Para calcular tiempo
double dwalltime(){
        double sec;
        struct timeval tv;

        gettimeofday(&tv,NULL);
        sec = tv.tv_sec + tv.tv_usec/1000000.0;
        return sec;
}

void imprimirMatriz(double *aux, int lado){
  int i,j;
  for (i = 0; i < lado; i++){
    for (j = 0; j < lado; j++){
      printf("%.1f\t", aux[i*N+j] );
    }
    printf("\n");
  }
}

void imprimirMatrizTriangular(double *aux, int lado){
  int i,j;
  for (i = 0; i < lado; i++){
    for (j = 0; j < lado; j++){
      if(i>=j){
        printf("%.1f\t", aux[i+j*N-((j*(j+1))/2)] );
      }
      else
        printf("0.0\t");
    }
    printf("\n");
  }
}

int main(int argc,char* argv[]){

 double *M,*L,*C;
 int i,j,k;
 int check = 1;
 double timetick;
 int H;

 //Controla los argumentos al programa
 if ((argc != 2) || ((N = atoi(argv[1])) <= 0) ) {
    printf("\nUsar: %s n\n  n: Dimension de la matriz (nxn X nxn)\n", argv[0]);
    exit(1);
  }

 H = (N*N-(N*(N-1))/2);
 M=(double*)malloc(sizeof(double)*N*N);
 L=(double*)malloc(sizeof(double)*H);
 C=(double*)malloc(sizeof(double)*N*N);

 for(i=0;i<N;i++){
  for(j=0;j<N;j++){
  M[i*N+j]=rand()%10;
  C[i*N+j]=0;
  }
 }

//Icializamos L
//for(i=0;i<H;i++){
  //L[i]= 1;  // Tenemos H posiciones, porque iria hasta N*N posiciones si me paso
  // N*N es mayor a H
  /* podria hacerlo como lo tenian ustedes, pero deberia implementarlo conuna condicion
     verificar las posiciones validas, es decir, L es una matriz triangular inferior,
     por tanto i=3,j=0 es una posicion de la matriz en donde tenia 0 y no la reserve,
     entonces en esa posicion no deberia guardar nada, entonces uso
      '''
      if(i>=j)
        L[i+j*N-((j*(j+1))/2)]=M[i*N+j]; // en L accedo asi porque L ira ordenado por columnas
      '''
     */
// }
 //Inicializamos L a partir de M
for (i = 0; i < N; i++) {
  for (j = 0; j < N; j++) {
    if(i>=j){
      L[i+j*N-((j*(j+1))/2)]= 3;
    }
  }
}

 imprimirMatriz(M,N);
 printf("\n");
 printf("\n");
 imprimirMatrizTriangular(L,N);
/* Realiza la multiplicacion */
 timetick = dwalltime();

 for(i=0;i<N;i++){
  for(j=0;j<N;j++){
   for(k=j;k<N;k++){ // para la multiplicacion tengo que modificar el acceso de K segun la dimension de la matriz,
                      // sea triangular inferior o superior
    C[i*N+j] += M[i*N+k]*L[i+k*N-((k*(k+1))/2)];
    /*
    if(i<k){
      C[i*N+j] += 0;
    }else{
        C[i*N+j] += M[i*N+k]*L[i+k*N-((k*(k+1))/2)];
    }*/

   }
  }
 }


 printf("\n");
 printf("\n");
 imprimirMatriz(C,N);

 printf("Tiempo en segundos %f\n", dwalltime() - timetick);


 free(M);
 free(L);
 free(C);

 return(0);
}
