#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

int N;

//Para calcular tiempo
double dwalltime(){
        double sec;
        struct timeval tv;

        gettimeofday(&tv,NULL);
        sec = tv.tv_sec + tv.tv_usec/1000000.0;
        return sec;
}

int main(int argc,char* argv[]){

 double *M,*L,*C;
 int i,j,k;
 double timetick;

 //Controla los argumentos al programa
 if ((argc != 2) || ((N = atoi(argv[1])) <= 0) ) {
    printf("\nUsar: %s n\n  n: Dimension de la matriz (nxn X nxn)\n", argv[0]);
    exit(1);
  }

 //Aloca memoria para las matrices
 M=(double*)malloc(sizeof(double)*N*N);
 L=(double*)malloc(sizeof(double)*N*N);
 C=(double*)malloc(sizeof(double)*N*N);

 //Inicializa las matrices
 //La matriz M se inicializa todas las columnas en 0 menos la ultima que la inicializa en
 //La matriz U se inicializa triangular inferior con N
 //La matriz C se inicializa en 0
 for(i=0;i<N;i++){
  for(j=0;j<N;j++){
    M[i+j*N]=rand()%10;
    C[i*N+j]=0;
   if(i>=j){
    L[N*i+j]=rand()%10;
   }else{
    L[N*i+j]=0;	//Esta almacenando los ceros
   }
  }
  }


 //Realiza la multiplicacion
 timetick = dwalltime();

 for(i=0;i<N;i++){
  for(j=0;j<N;j++){
   for(k=0;k<N;k++){
    C[i*N+j]=C[i*N+j] + L[k*N+j]*M[i+k*N];
   }
  }
 }

 printf("Tiempo en segundos %f\n", dwalltime() - timetick);


 free(M);
 free(L);
 free(C);

 return(0);
}
